package Ejercicio1;

import static Ejercicio1.Juego.fichero;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class Main {
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerConfigurationException, TransformerException {
        ArrayList<Juego> juegos=new ArrayList();
       //   java.nio.file.Path ruta = Paths.get(System.getProperty("user.dir"));
         // String fichero = ruta.resolve("juegos.xml").toString(); No sirve
         String fichero = "C:\\Users\\carlo\\Documents\\NetBeansProjects\\Practica2\\src\\Ejercicio1\\juegos.xml"; //recordar cambiar la direccion del fichero, ya que no me fuciona el Path
         DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
             
         try{ 
             Juego.Listar(fichero,factory);
                       DocumentBuilder builder = factory.newDocumentBuilder(); 
                        Document document = builder.parse(new File(fichero)); 
                        juegos=Juego.getJuegos();


        Scanner s = new Scanner(System.in);
        boolean b=true;
        while (b==true){ //Se muestra el menu cada vez que acabe la actividad (excepto 4)
           
        System.out.println("Introducir el valor numerico de lo que se quiere hacer");
        System.out.println("1. Listar XML");
        System.out.println("2. Añadir Juego");
        System.out.println("3. Eliminar Juego");
        System.out.println("4. Cerrar Programa");
        String ele=s.next();

        
        switch (ele){
            case "1": //Se muestran los juegos del ArrayList
                for (Juego item:juegos){
                    System.out.println("->");
                    System.out.println("Titulo: "+item.titulo);
                    System.out.println("Genero: "+item.genero);
                    System.out.println("Plataforma: "+item.plataforma);
                    System.out.println("Año: "+item.año);
                }

                break;
            case "2": 
                Scanner sc = new Scanner(System.in);
                boolean aux = false;
                System.out.println("-> Datos del nuevo Juego:");
                
		String titulo, genero,plataforma, año;
                	System.out.println("Inserta Titulo del Juego");
			titulo = sc.nextLine();
                        
                        for (Juego item:juegos){ //verifico si el juego ya existe
                            if (item.titulo.toLowerCase().equals(titulo.toLowerCase()))
                                aux=true;
                        }
                        
                        if (aux==false){     //si el juego no existe
			System.out.println("Inserta Genero del Juego");
			genero = sc.nextLine();
			System.out.println("Inserta Plataforma del juego");
			plataforma = sc.nextLine();
			System.out.println("Inserta Año de Lanzamiento del Juego");
			año = sc.nextLine();
                        
                        Element raiz = document.createElement("juego"); //crea la pestaña raiz juego
			document.getDocumentElement().appendChild(raiz);

                        
                        Element elem = document.createElement("titulo"); //creaa la pestaña titulo
			Text text = document.createTextNode(titulo);     //se le da valor
			raiz.appendChild(elem);                          //se agrega el elemeto "titulo" a la raiz "juego"
			elem.appendChild(text);                          //se le asigna el valor
                        //se hace lo mismo para cada pestaña
                        Element elem1 = document.createElement("genero"); 
			Text text1 = document.createTextNode(genero); 
			raiz.appendChild(elem1); 
			elem1.appendChild(text1); 
                        
                        Element elem2 = document.createElement("plataforma"); 
			Text text2 = document.createTextNode(plataforma);  
			raiz.appendChild(elem2); 
			elem2.appendChild(text2); 
                        
                        Element elem3 = document.createElement("fechadelanzamiento"); 
			Text text3 = document.createTextNode(año);  
			raiz.appendChild(elem3);  
			elem3.appendChild(text3); 
                        
                        Source source = new DOMSource(document); 
			Result result = new StreamResult(new java.io.File(fichero)); 
			Transformer transformer = TransformerFactory.newInstance() .newTransformer(); 
			transformer.transform(source, result);
                        Juego.Listar(fichero,factory); //Actualizo Lista de Juegos
                        juegos=Juego.getJuegos(); //get de Lista nueva de Juegos
                        System.out.println("Juego Añadido Exitosamente");
                        }
                        else //si el juego existe
                            System.err.println("El Juego que intenta ingresar ya existe en la base de datos");
                        break;
            case "3":
                Scanner st = new Scanner(System.in);
                System.out.println("-> Inserta el titulo del juego que quieres borrar.");
	        String borrar = st.nextLine();
                boolean aux1 = false;
                
                for (Juego item:juegos){ //verifico si existe el juego ingresado
                    if (item.titulo.toLowerCase().equals(borrar.toLowerCase()))
                    aux1=true;
                }
                if (aux1==true){ //si existe
                
                NodeList juegot = document.getElementsByTagName("juego");
			for (int i = 0; i < juegot.getLength(); i ++) { 
				Node juegon = juegot.item(i); //obtener un nodo 
				if (juegon.getNodeType() == Node.ELEMENT_NODE) {//tipo de nodo 
					Element elemento = (Element) juegon; //obtener los elementos del nodo
                                        
                                        NodeList nodo= elemento.getElementsByTagName("titulo").item(0).getChildNodes(); 
					Node valornodo = (Node) nodo.item(0); 
					if(valornodo.getNodeValue().toLowerCase().equals(borrar.toLowerCase())) { //verifico si los titulos son iguales
						juegon.getParentNode().removeChild(juegon); //se borra el juego
					}
				}
			}
                        Source source2 = new DOMSource(document); 
			Result result2 = new StreamResult(new java.io.File(fichero)); 
                        Transformer transformer1 = TransformerFactory.newInstance() .newTransformer(); 
			transformer1.transform(source2, result2);
                        
                        Juego.Listar(fichero,factory); //Actualizo Lista de Juegos
                        juegos=Juego.getJuegos(); //get de Lista nueva de Juegos
                        
                        
                        System.out.println("Juego eliminado Exitosamente");
                        
			
			
                }else //si no existe
                    System.err.println("No se han encontrado Coincidencias");
                        break;
            case "4":
                b=false;
                break;
                
            default:
                System.err.println("Ingresar numero del 1 al 4");
                
        }
        }
             }catch (Exception e) {
			          System.err.println("error");
         }
}
}



