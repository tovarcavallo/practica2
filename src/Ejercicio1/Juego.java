/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio1;


import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
/**
 *
 * @author carlo
 */
public class Juego {
    String titulo;
    String genero;
    String plataforma;
    String año;
    static ArrayList juegos = new ArrayList<Juego>();

    public static ArrayList getJuegos() {
        return juegos;
    }
    static String fichero = "C:\\Users\\carlo\\Documents\\NetBeansProjects\\Practica2\\src\\Ejercicio1\\juegos.xml";
    
    public Juego(String titulo, String genero, String plataforma, String año){ //constructor de juegos
        this.titulo=titulo;
        this.genero=genero;
        this.plataforma=plataforma;
        this.año=año;
        juegos.add(this);
    }
    public static void Listar(String fichero, DocumentBuilderFactory factory) throws SAXException, IOException, ParserConfigurationException{  //Borra y actualiza la lista desde 0 cada vez que es llamada
        try{                                                                                                                                   //recibe la direccion del fichero y la factoria
            
        juegos.clear();
        DocumentBuilder builder = factory.newDocumentBuilder(); 
		        Document document = builder.parse(new File(fichero)); 
			document.getDocumentElement().normalize();
                        System.out.println("Elemento raíz: " + document.getDocumentElement() .getNodeName());
                        
                        NodeList juego = document.getElementsByTagName("juego");
                        
                        for (int j = 0; j < juego.getLength(); j ++) { 
                            
				Node jue = juego.item(j); //obtener un nodo 
				
				if (jue.getNodeType() == Node.ELEMENT_NODE) {//tipo de nodo 
					Element elemento = (Element) jue; //obtener los elementos del nodo 
					NodeList nodo= elemento.getElementsByTagName("titulo").item(0).getChildNodes(); 
                                        NodeList nodo1 = elemento.getElementsByTagName("genero").item(0).getChildNodes();
                                        NodeList nodo2= elemento.getElementsByTagName("plataforma").item(0).getChildNodes(); 
                                        NodeList nodo3 = elemento.getElementsByTagName("fechadelanzamiento").item(0).getChildNodes();
					Node valornodo = (Node) nodo.item(0);
                                        Node valornodo1= (Node) nodo1.item(0);
                                        Node valornodo2 = (Node) nodo2.item(0);
                                        Node valornodo3= (Node) nodo3.item(0);
                                        Juego jueguito = new Juego(valornodo.getNodeValue(),valornodo1.getNodeValue(),valornodo2.getNodeValue(),valornodo3.getNodeValue());
                                }
                        }
        
    }catch (Exception e) {
			e.printStackTrace();
    

    

}
    }
    

}
