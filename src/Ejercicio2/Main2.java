/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio2;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author carlo
 */
public class Main2 {
    	public static void main(String[] args) {
		XMLReader procesadorXML;
		try {
			procesadorXML = XMLReaderFactory.createXMLReader(); //se crea el reader
			Gestionar gestor= new Gestionar(); //override de salida de elementos
			procesadorXML.setContentHandler(gestor); 
			InputSource fileXML = new InputSource("C:\\Users\\carlo\\Documents\\NetBeansProjects\\Practica2\\src\\Ejercicio2\\gamers.xml"); //Recordar cambiar la dirección del file
			procesadorXML.parse(fileXML) ; //se lee el archivo indicado
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		

	}
    
}
class Gestionar extends DefaultHandler {
	public Gestionar() {
		super(); 
	}

	public void startDocument() { 
		System.out.println("Comienzo del XML:");
	}

	public void endDocument() { 
		System.out.println("Final del XML");
	}

	public void startElement(String uri, String nombre, 
			String nombreC, Attributes atts) { 
		System.out.println("\tComienzo elemento: " + nombre); 
	} 

	public void endElement(String uri, String nombre, String nombreC){ 
		System.out.println("\t--Fin Elemento: " + nombre + "--"); 
	}

	public void characters(char[] ch, int inicio, int longitud) throws SAXException { 
		String car=new String(ch, inicio, longitud); 
		car = car.replaceAll("[\t\n]","");//quitar saltos de linea 
		if(!car.isEmpty())System.out.println ("\t\tCaracteres: " + car); 
	}
} 
