/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio3;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author carlo
 */
public class Main3 {
        public static void main(String[] args) throws IOException {
    		Cafe c1 = new Cafe("CafeIESCE",5.3,43,57); //Creo Cafes
                Cafe c2 = new Cafe("Cesur",1.5,34,54);
                Cafe c3;
                Cafe c4;


		
		XStream xstream = new XStream(new DomDriver());
                xstream.alias("cafe", Cafe.class);
                System.err.println("C1 Y C2");
		String xml = xstream.toXML(c1);   //Convierto Cafes en xml
                String xml2 = xstream.toXML(c2);
                
                System.out.println(xml);    //Imprimo cafes
                System.out.println(xml2);
                
	        c3 = (Cafe) xstream.fromXML(xml);  //Asigno valores de c1 a c3 extrayendo datos del xml
                c4 = (Cafe) xstream.fromXML(xml2); //Asigno valores de c2 a c4 extrayendo datos del xml
                
                System.err.println("C3 Y C4");   //Imprimo Cafes
                String xml3 = xstream.toXML(c3);
                String xml4 = xstream.toXML(c4);
		System.out.println(xml3);
                System.out.println(xml4);
                


}
}
