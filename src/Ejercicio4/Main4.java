/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio4;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author carlo
 */
public class Main4 {
    public static void main(String[] args) throws IOException {
        ArrayList<Cafe> cafes = new ArrayList();
        Cafe c1 =new Cafe("CafeIESCE",5.3,43); //Creo Cafes
        Cafe c2 =new Cafe("CafeIESSanFer",6.8,45);
        cafes.add(c1);
        cafes.add(c2);
        Proveedor pr =new Proveedor("150","Mi proovedor","mi calle","madrid","españa","28050","importacion",cafes); //Creo Proveedor
        XStream xstream = new XStream(new DomDriver());
        xstream.alias("cafe", Ejercicio4.Cafe.class); //Asingo Alias a Etiquetas
        xstream.alias("proveedor", Ejercicio4.Proveedor.class);
        
        xstream.aliasAttribute(Proveedor.class,"identificador", "150"); //Asigno los atributos del proveedor
        xstream.aliasField("cif", Proveedor.class, "identificador");
        xstream.aliasAttribute(Proveedor.class,"nombre", "Mi proveedor");
        xstream.aliasField("empresa", Proveedor.class, "nombre");
        
        String xml = xstream.toXML(pr);
        System.out.println(xml);
        
        Proveedor pr2 =(Proveedor) xstream.fromXML(xml); //Asigno valores de pr a pr1 extrayendo datos del xml
        System.out.println(xstream.toXML(pr2));
        
        
    }
}
