/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio4;

import java.util.ArrayList;

/**
 *
 * @author carlo
 */
public class Proveedor {
    String calle;
    String ciudad;
    String pais;
    String cp;
    String identificador;
    String nombre;
    String esNacional;
    ArrayList<Cafe> cafes;
    
    public Proveedor(String identificador,String nombre,String Calle,String Ciudad, String Pais, String cp, String esNacional, ArrayList cafes){
        this.identificador=identificador;
        this.nombre=nombre;
        this.calle=Calle;
        this.ciudad=Ciudad;
        this.pais=Pais;
        this.cp=cp;
        this.esNacional=esNacional;
        this.cafes=cafes;
        
    }
}
