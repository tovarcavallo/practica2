/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio5;

import java.util.ArrayList;

public class Alumno {
    String nombre;
    String apellidos;
    String fecha;
    String calle;
    String numero;
    Domi domicilio;
    
    public Alumno(String nombre, String apellidos, String fecha, String calle, String numero){
        this.nombre=nombre;
        this.apellidos=apellidos;
        this.fecha=fecha;
        Domi domix = new Domi(calle,numero);
        this.domicilio=domix;

        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Alumno{" + "nombre=" + nombre + ", apellidos=" + apellidos + ", fecha=" + fecha + ", calle=" + calle + ", numero=" + numero + ", domicilio=" + domicilio + '}';
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Domi getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(Domi domicilio) {
        this.domicilio = domicilio;
    }
    
    
}
